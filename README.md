# Quantum Architecture and Reinforcement Learning

## Team Reinforce

# Pitch deck

<img src="./pitch/1.png" />
<img src="./pitch/2.png" />
<img src="./pitch/3.png" />
<img src="./pitch/4.png" />
<img src="./pitch/5.png" />
<img src="./pitch/6.png" />
<img src="./pitch/7.png" />
<img src="./pitch/8.png" />
<img src="./pitch/9.png" />
<img src="./pitch/10.png" />

# Service Goal

To create Reinforcement Learning library and service for problem solving in quantum computation.
- Offer quantum circuit models for analysis and problem solving

<a href="Team_Reinforce_Quantum_GHZ_model_RL_analysis.ipynb">Quantum GHZ RL model analysis</a>

Environment and restriction of actions has to be planned and programmed per case.
- Learning requires at least 10.000s of simulation loops.
- Fast simulation learning cycle is mandatory.
- Actor to Critic and Deep Query network

We have successfully created a learning method for Reinforcement learning leveraging 
fidelity target as a learning model. What can we do with this?

# Relevance

Building and optimizing a quantum circuit is not a trivial task. We are looking for customer problems that require complex problem solving where we can utilize Reinforcement Learning techniques.

<a href="https://github.com/iqm-finland/iqm-academy-womanium-hackathon-DAQC-VQE">IQM Ansatz Challenge</a>
### IQM Reinforcement learning Solution example
- Apply Reinforcement learning to the Ansatz circuit operations by adding a parametrized variation to the Reinforcement environment and action.

<a href="https://github.com/womanium-quantum/Predict-the-orbit-of-the-James-Webb-space-telescope-with-a-quantum-algorithm---Herman-Kolden/blob/main/Challenge%20_%20End-to-end%20quantum%20simulation%20of%20space%20telescopes%20(feat.%20teleportation).pdf">James Webb Challenge</a>
### NASA Reinforcement learning Solution example
- The work would require to explore the HHL algorithm and also to look how the UniversalQCompiler compiles the algorithm to analyse parts that could be optimized even more.

<a href="https://github.com/womanium-quantum/Random-number-generation-using-boson-sampling---ORCA-Computing/blob/main/ORCA_Womanium.pdf">ORCA Computing Challenge</a>
### ORCA Computing Reinforcement learning Solution example
- Orca random number generation could be explored more in AI and reinforcement learning methods at the neural network level where random numbers are heavily used.
- How to leverage random number generation for the reinforcement learning process and neural network

<a href="https://github.com/womanium-quantum/Random-number-generation-using-boson-sampling---ORCA-Computing/blob/main/ORCA_Womanium.pdf">ORCA Computing Challenge</a>
### Deloitte Green Qupermarket Reinforcement learning Solution example
- Reinforcement learning method builds a prediction model that predicts energy provided by the solar panels. Data is known for the weather/energy output a week in advance.
- AI model predicts the the energy output of the solar panels. 
- How much energy I need to store and how much I should put into cars.
- Quantum photonic techniques are used for Reinforcement learning model's random number generation with ORCA Computing techniques.

<a href="https://github.com/womanium-quantum/Quantum-Natural-Language-Processing-with-lambeq---Quantinuum/blob/main/Hackathon_QNLP_challenge.pdf">Quantinuum QNLP Ansatz Challenge</a>
### Quantinuum Reinforcement learning Solution example
- Apply Reinforcement learning to the Ansatz circuit operations by adding a parametrized variation to the Reinforcement environment and action.

<a href="https://github.com/womanium-quantum/Quantum-approximate-optimisation-algorithms-for-real-world-scenarios---Strangeworks/blob/main/Intro.ipynb">Strangeworks QAOA Challenge</a>
### Strangeworks Reinforcement learning Solution example
- Apply Reinforcement learning to the QAOA circuit operations by adding a parametrized variation to the Reinforcement environment and action.
- Use the gate cost table also in reinforcement learning environment to optimize the cost of a circuit run.

# Difficulty level

The most challenging part is to create a well working reward function and to define and map neural network structure to the problem solving.

# References

Quantum Architecture Search via Deep Reinforcement Learning
https://arxiv.org/abs/2104.07715 

IBM Qiskit Summer School 2022 initial problem solving with Reinforcement learning techniques.

### Team member certificated and badges
- <a href="https://www.credly.com/badges/ed1c2932-6585-485c-9920-931d8ba43505/public_url">Tuomas Sorakivi Qiskit Global Summer School 2022 - Quantum Excellence Badge</a>
- <a href="https://www.credly.com/badges/7729a34f-9310-493b-a32e-863faa53373b/public_url">Tuomas Sorakivi IBM Quantum Spring Challenge 2022 Achievement</a>
- Soham Bopardikar IBM Qiskit Advocate 

### Pitch presenter
- Tuomas Sorakivi Discord: Tuomas#8634, Email: tuomas@gigabrain.io, GitLab username: @sortuo
# Tasks and output

- Build a business and service for quantum computing
- Define environment
- Define simulation
- Define reward function
- Define usage of a neural network or other technique approach
- Create an IBM Qiskit module
- Create a neural network to solve the problem

Output is the deep learning neural network that has been taught to solve a certain problem given by the customer. 
